.common_limits: &common_limits
  limits:
    most_recent: 100

.common_summary_rule_actions: &common_summary_rule_actions
  item: |
    - [ ] #{full_resource_reference}+ (by `{{author}}`) - {{labels}}

resource_rules:
  merge_requests:
    summaries:
      - name: Daily Community Contributions Report
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Community contributions report
            summary: |
              Hi MR coaches! This report consolidates all community contributions requiring attention. Please help process these merge requests according to our workflow guidelines.

              ## Quick Links
              - [In Dev MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20dev)
              - [Ready for Review MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Aready%20for%20review)
              - [In Review MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20review)
              - [Blocked MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ablocked)
              - If the merge request depends on the completion of work elsewhere, set ~"workflow::blocked".

              ## Required Actions
              For each merge request, please:
              - If reviewer assigned + last comment from reviewer: Ping author, set ~"workflow::in dev"
              - If reviewer assigned + last comment from author: Ping reviewer, set ~"workflow::ready for review"
              - If no reviewer is assigned, find and assign a reviewer via one of the below:
                - Danger bot's suggested reviewer
                - [GitLab Roulette](https://gitlab-org.gitlab.io/gitlab-roulette/)
                - [Product Categories](https://handbook.gitlab.com/handbook/product/categories/)
              - If blocked by dependencies: Set ~"workflow::blocked"

              For more details, see [Community Contributions Handbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows).

              #{
              group = Hierarchy::Group.new("contributor_success")
              short_team_summary(group_key: "contributor_success", title: "## Community Contribution Report", extra_assignees: group.mentions + ["@zillemarco", "@taucher2003"], greet: false)
              }
        rules:
          - name: 1. First-time contributor merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - 1st contribution
              forbidden_labels:
                - idle
                - stale
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
                - "group::runner"
                - "group::hosted runners"
                - "group::distribution"
              ruby: |
                IdleMrHelper.idle?(resource, days: 7)
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|devops::|group::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"1st contribution" Merge Requests (7+ days idle)
                  These MRs require immediate attention to ensure a positive first experience.

                  {{items}}
          - name: 2. General idle merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
                - "group::runner"
                - "group::hosted runners"
                - "group::distribution"
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|devops::|group::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} General Idle MRs (21+ days without activity)
                  Community contributions requiring GitLab response.

                  {{items}}
          - name: 3. Distribution merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - "group::distribution"
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
              ruby: |
                resource[:labels].include?('idle') || (resource[:labels].include?('1st contribution') && IdleMrHelper.idle?(resource, days: 7))
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"group::distribution" MRs
                  ~"group::distribution" follows a [different review workflow](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/merge_requests/#workflow).
                  **Do not modify workflow labels.**

                  If MRs appear stuck, ping assigned reviewer or `@gitlab-org/distribution` if none assigned.

                  {{items}}
          - name: 4. Runner merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
              ruby: (resource[:labels] & ['group::runner', 'group::hosted runners']).any?
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"group::runner" and ~"group::hosted runners" MRs (21+ days without activity)
                  {{items}}
          - name: 5. Untriaged merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
              ruby: untriaged? && [14292404, 11511606].exclude?(resource[:project_id])
            <<: *common_limits
            actions:
              summarize:
                destination: gitlab-org/quality/triage-reports
                <<: *common_summary_rule_actions
                title: |
                  #{Date.today.iso8601} Untriaged community merge requests
                summary: |
                  ## Untriaged Community MRs

                  Required labels:
                  1. [Type label](https://docs.gitlab.com/ee/development/labels/#type-labels)
                  2. [Group label](https://docs.gitlab.com/ee/development/labels/#group-labels)
                    1. Some group labels are not sufficient (e.g. ~"group::distribution"), and you will need to use the lower level labels such as ~"group::distribution::build")
                  3. [Stage label](https://docs.gitlab.com/ee/development/labels/#stage-labels)
                    1.  If no appropriate stage and group label, add a [department or team label](https://docs.gitlab.com/ee/development/labels/#department-labels).
                  4. [Category labels](https://docs.gitlab.com/ee/development/labels/#category-labels) facilitate automatic addition of stage and group labels

                  ### Security Note
                  If an MR appears to be fixing a security vulnerability that you believe should not be public, please ask the AppSec team in the sec-appsec Slack channel.

                  ### Quick Setup
                  The quick action `/copy_metadata <issue link>` can fill in the necessary information.
                  Always verify the metadata accuracy.


                  ### Reviewer Assignment Process

                  1. **For MRs with Group Label:**
                    - Assign Danger bot's suggested reviewer or
                    - Assign a reviewer from the [product group](https://handbook.gitlab.com/handbook/product/categories/) or
                    - Escalate to the group's engineering manager if team is at capacity

                  2. **For MRs with Uncertain Ownership:**
                    - Assign Danger bot's suggested reviewer or
                    - Assign a reviewer through [reviewer roulette](https://gitlab-org.gitlab.io/gitlab-roulette/)
                  - Note: Any qualified reviewer can help - MR coach status not required

                  ### Completion Steps

                  1. Check off processed MRs
                  2. Once you've triaged all the merge requests assigned to you, you can unassign and unsubscribe yourself:
                  ```
                  /done
                  /unassign me
                  /unsubscribe
                  ```

                  Alternatively, you can use the existing project [comment template](https://docs.gitlab.com/ee/user/profile/comment_templates.html) or create a custom one with the above quick actions.

                  **When all the checkboxes are done, close the issue, and celebrate!** :tada:

                  #{ coaches = merge_request_coaches; nil }
                  #{ distribute_and_display_items_per_triager(resource[:items].lines(chomp: true), coaches) }

                  ---

                  Job URL: #{ENV['CI_JOB_URL']}
                  This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/community-contribution-report.yml)

                  /label ~"triage report" ~"Community contribution"
