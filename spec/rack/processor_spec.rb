# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/processor'

RSpec.describe Triage::Rack::Processor do
  let(:app)      { described_class.new }
  let(:payload)  { { "foo" => "bar" } }
  let(:env)      { { Rack::RACK_INPUT => StringIO.new(JSON.dump(payload)) } }

  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(Triage::ProcessorJob).to receive(:perform_async).with(payload)
  end

  it 'passes parsed payload to the ProcessorJob worker' do
    expect(Triage::ProcessorJob).to receive(:perform_async).with(payload)

    expect(response).to be_an(Array)
  end

  it 'returns a 200 response' do
    expect(body).to eq(JSON.dump({ status: :ok }))
    expect(status).to eq(200)
  end

  context 'when request payload is invalid JSON' do
    let(:env) { { Rack::RACK_INPUT => StringIO.new("foo") } }

    it 'returns a 400 error' do
      expect(status).to eq(400)
      expect(body).to eq(JSON.dump({ status: :error, error: "JSON::ParserError", message: "unexpected token at 'foo'" }))
    end
  end

  context 'when ProcessorJob.perform_async raises exception' do
    before do
      allow(Triage::ProcessorJob).to receive(:perform_async).and_raise(JSON::ParserError)
    end

    it 'bubbles up the exception' do
      expect do
        response
      end.to raise_error(JSON::ParserError)
    end
  end
end
