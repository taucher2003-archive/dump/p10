# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/missed_resource_helper'

RSpec.describe MissedResourceHelper do
  let(:milestone) { double(:milestone, due_date: due_date, title: '12.5') }
  let(:due_date) { Date.new(2019, 11, 17) }

  let(:resource_klass) do
    Struct.new(:issue) do
      include MissedResourceHelper
    end
  end

  subject { resource_klass.new }

  describe '#missed_resource?' do
    it 'returns false if milestone due date is nil' do
      expect(subject.missed_resource?(nil)).to be false
    end

    it 'returns false on prior date of milestone due date' do
      expect(subject.missed_resource?(milestone, due_date - 1)).to be false
    end

    it 'returns false when date is the milestone due date' do
      expect(subject.missed_resource?(milestone, due_date)).to be false
    end

    it 'returns true when date is after the milestone due date' do
      expect(subject.missed_resource?(milestone, due_date + 1)).to be true
    end
  end

  describe '#add_missed_labels' do
    context 'when there is no Deliverable label' do
      it 'returns /label ~"missed:x.y"' do
        actions = subject.add_missed_labels('12.5', [])

        expect(actions).to eq('/label ~"missed:12.5"')
      end
    end

    context 'when there is a Deliverable label' do
      it 'returns /label ~"missed:x.y"' do
        actions = subject.add_missed_labels('12.5', %w[Deliverable])

        expect(actions).to eq(<<~ACTIONS.chomp)
          /label ~"missed:12.5"
          /label ~"missed-deliverable"
        ACTIONS
      end
    end
  end

  describe '#remove_deliverable_labels' do
    it 'returns /unlabel ~"Deliverable" ~"goal::x"' do
      actions = subject.remove_deliverable_labels

      expect(actions).to eq('/unlabel ~"Deliverable" ~"goal::stretch" ~"goal::planning" ~"goal::development" ~"goal::complete"')
    end
  end

  describe '#move_to_current_milestone' do
    context 'when the current milestone is 12.6' do
      let!(:versioned_milestone) { VersionedMilestone.new(subject) }

      before do
        allow(VersionedMilestone)
          .to receive(:new)
          .with(subject)
          .and_return(versioned_milestone)

        allow(versioned_milestone)
          .to receive_message_chain(:current, :title)
          .and_return('12.6')
      end

      it 'returns a quick command to set to 12.6' do
        actions = subject.move_to_current_milestone

        expect(actions).to eq('/milestone %"12.6"')
      end
    end
  end

  describe '#move_to_next_milestone' do
    context 'when the next milestone is 17.4' do
      let!(:versioned_milestone) { VersionedMilestone.new(subject) }

      before do
        allow(VersionedMilestone)
          .to receive(:new)
          .with(subject)
          .and_return(versioned_milestone)

        allow(versioned_milestone)
          .to receive_message_chain(:next, :title)
          .and_return('17.4')
      end

      it 'returns a quick command to set to 17.4' do
        actions = subject.move_to_next_milestone

        expect(actions).to eq('/milestone %"17.4"')
      end
    end
  end
end
