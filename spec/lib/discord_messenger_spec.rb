# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/discord_messenger'

RSpec.describe DiscordMessenger do
  let(:webhook_path) { nil }

  subject { described_class.new(webhook_path) }

  describe '#send_message' do
    let(:message) { 'test' }

    let!(:request) do
      stub_request(:post, "#{DiscordMessenger::BASE_URL}/#{webhook_path}")
        .with(body: hash_including('content' => message))
    end

    context 'when the provided webhook path is nil' do
      it 'does not send any API request' do
        subject.send_message(message)

        expect(request).not_to have_been_requested
      end
    end

    context 'when the provided webhook path is not nil' do
      let(:webhook_path) { 'test_test' }

      it 'sends the API request' do
        subject.send_message(message)

        expect(request).to have_been_requested
      end
    end
  end
end
