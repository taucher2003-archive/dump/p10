# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/pipeline_incident_helper'

RSpec.describe PipelineIncidentHelper do
  let(:duplicate_incident_url) { 'api/v4/projects/40549124/issues/5332' }

  let(:resource) do
    {
      '_links' => { 'closed_as_duplicate_of' => duplicate_incident_url }
    }
  end

  describe '.closed_as_duplicate_of' do
    subject(:duplicate_url) { described_class.closed_as_duplicate_of(resource) }

    it { is_expected.to be(duplicate_incident_url) }
  end

  describe '.apply_root_cause_label_from_duplicate_incident' do
    let(:issue_double) { instance_double('Issue', labels: ['master-broken::flaky-test']) }

    subject(:comment) { described_class.apply_root_cause_label_from_duplicate_incident(resource) }

    before do
      allow(Triage.api_client).to receive(:issue).and_return(issue_double)
    end

    it { is_expected.to eq('/label ~"master-broken::flaky-test"') }
  end
end
