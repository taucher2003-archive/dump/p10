# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/schedule/manager'

RSpec.describe Schedule::Manager do
  let(:api_token) { 'token123' }
  let(:dry_run_schedule) { nil }
  let(:specification_file_path) { File.expand_path('../../../pipeline-schedules.yml', __dir__) }

  subject do
    described_class.new(
      described_class::Options.new(api_token, false, dry_run_schedule))
  end

  describe '#schedules_specification' do
    it 'ensures a pipeline-schedules.yml file exists at the root of the project' do
      expect(File).to exist(specification_file_path)
    end

    it 'reads from the pipeline-schedules.yml file' do
      expect(subject.schedules_specification).to be_an(Array)
      expect(subject.schedules_specification).to all(be_a(Schedule::Variant))
    end
  end

  describe '#run!' do
    context 'when dry_run_schedule is set correctly' do
      let(:dry_run_schedule) { 'gitlab-org' }
      let(:schedules) { YAML.load_file(specification_file_path) }
      let(:schedule_variables) do
        schedules.dig(dry_run_schedule, 'base', 'variables')
          .merge(schedules.dig(dry_run_schedule, 'variants', 0, 'variables'))
          .merge(DRY_RUN: 1)
          .deep_symbolize_keys!
      end

      before do
        allow(ENV).to receive(:fetch).with('TRIAGE_TRIGGER_TOKEN').and_return('TOKEN')
        allow(ENV).to receive(:fetch).with('TRIAGE_SCHEDULE_INDEX')
        allow(ENV).to receive(:fetch).with('CI_COMMIT_REF_NAME').and_return('REF')
        allow(Gitlab).to receive_message_chain(:run_trigger, :web_url)
          .and_return('https://example.com/')
        allow(subject).to receive(:info)
        schedule_variables.merge!('PIPELINE_NAME' => '[HOURLY] gitlab-org label community contributions')
      end

      it 'finds the right schedule and trigger a pipeline with it' do
        expected_variables = schedule_variables

        subject.run!

        expect(Gitlab).to have_received(:run_trigger)
          .with(
            described_class::PROJECT_PATH, 'TOKEN', 'REF', expected_variables)
        expect(subject).to have_received(:info)
          .with(include('https://example.com/'))
      end

      context 'with a bypassing variable set' do
        before do
          result = {}

          allow(ENV).to receive(:each_with_object)
            .and_yield(['BYPASSING_ME', 'MEME!'], result).and_return(result)
        end

        it 'bypass the right variable' do
          expected_variables = schedule_variables.merge(ME: 'MEME!')

          subject.run!

          expect(Gitlab).to have_received(:run_trigger)
            .with(
              described_class::PROJECT_PATH, 'TOKEN', 'REF', expected_variables)
        end
      end
    end

    context 'when dry_run_schedule is not set correctly' do
      let(:dry_run_schedule) { 'bad-schedule-name' }

      it 'raises InvalidDryRunScheduleName' do
        expect do
          subject.run!
        end.to raise_error(described_class::InvalidDryRunScheduleName)
      end
    end

    context 'when dry_run_schedule is not set' do
      it 'calls sync!' do
        expect(subject).to receive(:sync!)

        subject.run!
      end
    end
  end

  describe '#sync!' do
    let(:schedule_klass) do
      Class.new(Struct.new(:description, :ref, :cron, :cron_timezone, :active, :variables, :id)) do
        def variables
          self[:variables] ||= []
        end
      end
    end

    let(:variable_klass) do
      Struct.new(:key, :value) do
        def to_params
          { key: key.to_s, value: value.to_s }
        end
      end
    end

    it 'calls #sync_expected_schedules! and #disable_extra_schedules!' do
      allow(subject).to receive(:info)
      expect(subject).to receive(:sync_expected_schedules!)
      expect(subject).to receive(:disable_extra_schedules!)

      subject.sync!
    end

    context 'when no token is given' do
      let(:api_token) { nil }

      it 'raises an ArgumentError error' do
        expect { subject.sync! }.to raise_error(ArgumentError, 'An API token is needed!')
      end
    end

    describe 'syncing process' do
      let(:api_client) { double(:api_client) }
      let(:remote_schedules) { double(auto_paginate: current_schedules) }
      let(:current_schedules) { [] }
      let(:raw_schedules_specification) { {} }

      before do
        allow(subject).to receive(:raw_schedules_specification).and_return(raw_schedules_specification)
        allow(subject).to receive(:api_client).and_return(api_client)
        allow(subject).to receive(:info)
        allow(api_client).to receive(:pipeline_schedules).and_return(remote_schedules)
      end

      context 'when existing schedule with correct settings and base description is given' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo' ('DAILY_AUTOMATION=1')", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('DAILY_AUTOMATION', '1')]) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                description: 'BASE DESCRIPTION IS IGNORED',
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { variables: { DAILY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages and ignores the base description' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo' ('DAILY_AUTOMATION=1')"))

          subject.sync!
        end
      end

      context 'when existing schedule with correct settings' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo' ('DAILY_AUTOMATION=1')", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('DAILY_AUTOMATION', '1')]) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { variables: { DAILY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo' ('DAILY_AUTOMATION=1')"))

          subject.sync!
        end
      end

      context 'when existing schedule with correct settings and a defined id' do
        let(:remote_schedule) { schedule_klass.new("OLD DESCRIPTION", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('DAILY_AUTOMATION', '1')], 42) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { id: remote_schedule.id, description: 'NEW DESCRIPTION', variables: { DAILY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(subject).to receive(:info).with("\t🏗 Attribute `description` value 'OLD DESCRIPTION' will be corrected to 'NEW DESCRIPTION'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { description: 'NEW DESCRIPTION' })

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: 'NEW DESCRIPTION'))

          subject.sync!
        end
      end

      context 'when existing schedule with correct settings and wrongly defined id' do
        let(:current_schedules) { [] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [
                { id: 1234 }
              ]
            }
          }
        end

        it 'raises an error' do
          expect { subject.sync! }.to raise_error(described_class::InvalidPipelineScheduleIdError, "💥 Schedule with ID #1234 couldn't be found! 💥")
        end
      end

      context 'when existing schedules have the same description' do
        let(:current_schedules) { [] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [
                { description: 'foo bar' },
                { description: 'foo bar' }
              ]
            }
          }
        end

        it 'raises an error' do
          expect { subject.sync! }.to raise_error(described_class::DuplicatePipelineSchedule, "💥 Multiple schedules were found for description 'foo bar'! 💥")
        end
      end

      context 'when existing schedule with an incorrect description compared to the specification' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-5', 'UTC', true, [], 42) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{ id: remote_schedule.id, description: 'NEW DESCRIPTION' }]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `description` value '[MANAGED] 'foo'' will be corrected to 'NEW DESCRIPTION'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { description: 'NEW DESCRIPTION' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: 'NEW DESCRIPTION'))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect ref compared to the default' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'feature', '0 0 * * 1-5', 'UTC', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{}]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `ref` value 'feature' will be corrected to 'master'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { ref: 'master' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect ref compared to the specification' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'feature', '0 0 * * 1-5', 'UTC', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{ ref: 'stable' }]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `ref` value 'feature' will be corrected to 'stable'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { ref: 'stable' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect cron compared to the default' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-7', 'UTC', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{}]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `cron` value '0 0 * * 1-7' will be corrected to '0 0 * * 1-5'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { cron: '0 0 * * 1-5' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect cron compared to the specification' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-7', 'UTC', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{ cron: '0 0 * * 1-3' }]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `cron` value '0 0 * * 1-7' will be corrected to '0 0 * * 1-3'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { cron: '0 0 * * 1-3' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect cron_timezone compared to the default' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-5', 'Pacific', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{}]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `cron_timezone` value 'Pacific' will be corrected to 'UTC'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { cron_timezone: 'UTC' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect cron_timezone compared to the specification' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-5', 'Pacific', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{ cron_timezone: 'Europe' }]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `cron_timezone` value 'Pacific' will be corrected to 'Europe'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { cron_timezone: 'Europe' })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect active compared to the default' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-5', 'UTC', false, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{}]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `active` value 'false' will be corrected to 'true'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { active: true })

          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect active compared to the specification' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo'", 'master', '0 0 * * 1-5', 'UTC', true, []) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              variants: [{ active: false }]
            }
          }
        end

        it 'prints messages and updates the schedule' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(subject).to receive(:info).with("\t🏗 Attribute `active` value 'true' will be corrected to 'false'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { active: false })
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo'"))

          subject.sync!
        end
      end

      context 'when existing schedule with an unknown variable' do
        let(:remote_schedule) { schedule_klass.new("[MANAGED] 'foo' ('DAILY_AUTOMATION=1')", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('DAILY_AUTOMATION', '1'), variable_klass.new('TEST', '1')]) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { variables: { DAILY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(subject).to receive(:info).with("\t🗑 Variable TEST=1 isn't a managed variable, we'll remove it. 🗑")
          expect(api_client).to receive(:delete_pipeline_schedule_variable).with(remote_schedule, 'TEST')
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: "[MANAGED] 'foo' ('DAILY_AUTOMATION=1')"))

          subject.sync!
        end
      end

      context 'when existing schedule with an incorrect variable' do
        let(:remote_schedule) { schedule_klass.new("schedule", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('WEEKLY_AUTOMATION', '0')]) }
        let(:current_schedules) { [remote_schedule] }
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { description: 'schedule', variables: { WEEKLY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages and updates the variable' do
          expect(subject).to receive(:info).with("ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️")
          expect(api_client).to receive(:pipeline_schedule).with(remote_schedule).and_return(remote_schedule)
          expect(subject).to receive(:info).with("\t🏗 Variable `WEEKLY_AUTOMATION` was found but its value '0' will be corrected to '1'. 🏗")

          expect(api_client).to receive(:edit_pipeline_schedule_variable).with(remote_schedule, 'WEEKLY_AUTOMATION', { value: '1' })
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(anything, a_hash_including(key: 'PIPELINE_NAME', value: 'schedule'))

          subject.sync!
        end
      end

      context 'when missing schedule' do
        let(:raw_schedules_specification) do
          {
            'foo' => {
              base: {
                variables: { TRIAGE_SOURCE_PATH: 'foo' }
              },
              variants: [
                { variables: { WEEKLY_AUTOMATION: 1 } }
              ]
            }
          }
        end

        it 'prints messages and creates the schedule and its variables' do
          new_schedule_params = { description: "[MANAGED] 'foo' ('WEEKLY_AUTOMATION=1')", **Schedule::Variant::DEFAULT_SCHEDULE_PARAMS }.sort.to_h
          expect(subject).to receive(:info).with("🏗 Schedule `#{new_schedule_params[:description]}` wasn't found, we'll create it. 🏗")

          new_schedule = schedule_klass.new
          expect(api_client).to receive(:create_pipeline_schedule).with(new_schedule_params).and_return(new_schedule)
          expect(api_client).to receive(:pipeline_schedule).with(new_schedule).and_return(new_schedule)

          new_schedule_variables_params = [
            variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'),
            variable_klass.new('WEEKLY_AUTOMATION', '1'),
            variable_klass.new('PIPELINE_NAME', "[MANAGED] 'foo' ('WEEKLY_AUTOMATION=1')")
          ]
          expect(subject).to receive(:info).with("\t🏗 Variable '#{new_schedule_variables_params[0].key}' wasn't found, we'll create it. 🏗")
          expect(subject).to receive(:info).with("\t🏗 Variable '#{new_schedule_variables_params[1].key}' wasn't found, we'll create it. 🏗")
          expect(subject).to receive(:info).with("\t🏗 Variable '#{new_schedule_variables_params[2].key}' wasn't found, we'll create it. 🏗")

          expect(api_client).to receive(:create_pipeline_schedule_variable).with(new_schedule, new_schedule_variables_params[0].to_params)
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(new_schedule, new_schedule_variables_params[1].to_params)
          expect(api_client).to receive(:create_pipeline_schedule_variable).with(new_schedule, new_schedule_variables_params[2].to_params)

          subject.sync!
        end
      end

      context 'when unknown schedule' do
        let(:remote_schedule) { schedule_klass.new("unknown schedule", 'master', '0 0 * * 1-5', 'UTC', true, [variable_klass.new('TRIAGE_SOURCE_PATH', 'foo'), variable_klass.new('WEEKLY_AUTOMATION', '0')]) }
        let(:current_schedules) { [remote_schedule] }

        it 'prints messages and disable the unknown schedules' do
          expect(subject).to receive(:info).with("⚠️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` isn't a managed schedule, it will be disabled. ⚠️")

          expect(api_client).to receive(:edit_pipeline_schedule).with(remote_schedule, { active: false })

          subject.sync!
        end
      end
    end
  end
end
