# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/quick_win_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::QuickWinLabel do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        weight: weight,
        added_label_names: labels_added,
        description: issue_description,
        from_gitlab_group?: from_gitlab_group
      }
    end

    let(:from_gitlab_group) { true }
    let(:labels_added) { ['quick win'] }
    let(:weight) { 1 }

    let(:issue_description) do
      <<~DESCRIPTION
        ## Implementation plan
        This is the implementation plan
      DESCRIPTION
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.open", "issue.reopen", "issue.update"]

  describe '#applicable?' do
    where(:from_gitlab_group, :labels_added, :weight, :issue_description, :expected) do
      [
        # Behaves as expected (from gitlab org/com/community/components)
        [true, ['quick win'], nil, '', 'event is applicable'],

        # Event is not from gitlab org, com, community, or components
        [false, ['quick win'], nil, '', 'event is not applicable'],

        # Event is not adding the quick win label
        [true, ['frontend'], nil, '', 'event is not applicable'],

        # Implementation plan provided, we check weight
        [true, ['quick win'], 5, "## Implementation plan\nPlan description", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\nPlan description", 'event is not applicable'],

        # Weight is correct, we check the implementation plan
        [true, ['quick win'], 1, 'short description', 'event is applicable'],
        [true, ['quick win'], 1, "# Implementation plan\n", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n\n\n", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n# First level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n## Second level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n### Third level heading", 'event is applicable'],
        [true, ['quick win'], 1, "### Implementation plan\n", 'event is applicable'],

        # Third level heading with content or extra headers
        [true, ['quick win'], 1, "## Implementation plan\n### Third level heading\nThis is the implementation plan", 'event is not applicable'],
        [true, ['quick win'], 1, "## Implementation plan\n### Third level heading\nThis is the implementation plan\n## Unrelated heading", 'event is not applicable'],
        [true, ['quick win'], 1, "## Implementation abc\nThis is the implementation plan", 'event is not applicable'],
        [true, ['quick win'], 1, "## Implementation\nThis is the implementation plan", 'event is not applicable']
      ]
    end

    with_them do
      it 'behaves as expected' do
        expect(subject).not_to be_applicable if expected == 'event is not applicable'
        expect(subject).to be_applicable if expected == 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts quick win guide comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          @root thanks for adding the ~"quick win" label!

          However, it appears that this issue does not meet all of the required criteria for the label to be applied
          so it has been automatically removed.

          You can refer to the [criteria for quick win issues documentation](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#criteria-for-quick-win-issues) for an up-to-date list of the requirements.

          /unlabel ~"quick win"
          /label ~"automation:quick-win-removed"
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
