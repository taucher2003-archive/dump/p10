# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/growth_affecting_notifier'

RSpec.describe Triage::GrowthAffectingNotifier do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        project_id: project_id,
        iid: merge_request_iid,
        from_gitlab_org?: true
      }
    end
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "something/old.md",
          "new_path" => "something/new.md"
        }
      ]
    }
  end

  let(:label_names) { ['Community contribution'] }
  let(:codeowners_paths) do
    [
      "/ee/app/workers/onboarding/",
      "/other/path/to/watch/"
    ]
  end

  include_context 'with merge request notes'

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes
    )
    allow(subject).to receive(:paths_for_owner).and_return(codeowners_paths)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when there is no match from codeowners' do
      include_examples 'event is not applicable'
    end

    context 'when there is a match from codeowners' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "/ee/app/workers/onboarding/test_file.haml",
              "new_path" => "something/new.md"
            }
          ]
        }
      end

      it_behaves_like 'community contribution open resource #applicable?'
    end

    context 'when there is a match from GROWTH_ALERT_FILES' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "app/views/admin/application_settings/_account_and_limit.html.haml",
              "new_path" => "something/new.md"
            }
          ]
        }
      end

      it_behaves_like 'community contribution open resource #applicable?'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          this merge request touches files that could potentially affect user growth or subscription cost management.
        MARKDOWN
      end

      expect_comment_request(event: event, body: include(body)) do
        subject.process
      end
    end
  end
end
