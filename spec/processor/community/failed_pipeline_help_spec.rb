# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/failed_pipeline_help'

RSpec.describe Triage::FailedPipelineHelp do
  include_context 'with event', Triage::PipelineEvent do
    let(:event_attrs) do
      {
        status: 'failed',
        merge_request_iid: merge_request_iid,
        payload: {
          'merge_request' => {
            'iid' => merge_request_iid,
            'target_project_id' => project_id
          }
        }
      }
    end

    let(:project_id) { 1 }
    let(:merge_request_iid) { 456 }
    let(:event_actor_username) { 'mr_author_username' }
    let(:label_names) { ['backend', 'Community contribution'] }
  end

  let(:latest_pipeline) { true }
  let(:merge_request_notes) { [] }

  subject(:failed_pipeline_help) { described_class.new(event) }

  before do
    allow(event).to receive(:latest_pipeline_in_merge_request?).and_return(latest_pipeline)
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
      response_body: {
        'iid' => merge_request_iid,
        'labels' => label_names,
        'assignees' => [{ 'username' => 'mr_author_username' }]
      }
    )
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/456/notes",
      query: { per_page: 100 },
      response_body: merge_request_notes
    )
  end

  include_examples 'registers listeners', ['pipeline.failed']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when MR is not wider community contribution' do
      let(:label_names) { ['backend'] }

      include_examples 'event is not applicable'
    end

    context "when it's not the latest pipeline in MR" do
      let(:latest_pipeline) { false }

      include_examples 'event is not applicable'
    end

    context "when pipeline was triggered not by MR assignee" do
      let(:event_actor_username) { 'mr_reviewer_username' }

      include_examples 'event is not applicable'
    end

    context 'when help comment already present' do
      let(:merge_request_notes) do
        [
          {
            body: '<!-- triage-serverless FailedPipelineHelp - Project ID 1 - MR ID 456 -->'
          }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:unique_message_identifier) { '<!-- triage-serverless FailedPipelineHelp - Project ID 1 - MR ID 456 -->' }
    let(:expected_message) do
      <<~MARKDOWN.concat("\n").chop.chop
        #{unique_message_identifier}
        Hey @#{event_actor_username}! Looks like your pipeline failed :confused:.
        If you need help fixing it, you can reply with: `@gitlab-bot help` to tag a merge
        request coach or you can ask for help in the #contribute channel on our
        [Discord community server](https://discord.gg/gitlab).
        \n\n
        *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
        [Improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/failed_pipeline_help.rb)
        or [delete it](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations/#reactive-delete_bot_comment-command).*
      MARKDOWN
    end

    it 'posts a comment to MR author and pings coach' do
      expect_comment_request(
        noteable_path: "/projects/#{event.project_id}/merge_requests/#{event.merge_request_iid}",
        body: expected_message
      ) do
        failed_pipeline_help.process
      end
    end
  end
end
