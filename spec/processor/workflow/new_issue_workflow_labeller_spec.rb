# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/new_issue_workflow_labeller'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::Workflow::NewIssueWorkflowLabeller do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        project_public?: project_public,
        label_names: label_names
      }
    end
  end

  let(:from_gitlab_org) { true }
  let(:project_public)  { true }
  let(:label_names)     { [] }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event project is not public' do
      let(:project_public) { false }

      include_examples 'event is not applicable'
    end

    context 'when no labels are set' do
      let(:label_names) { [] }

      include_examples 'event is applicable'
    end

    context 'when new workflow label needs adding' do
      let(:label_names) { ['workflow::problem validation'] }

      include_examples 'event is applicable'
    end

    context 'when new workflow label needs updating' do
      let(:label_names) { ['workflow::complete', 'issue::validation'] }

      include_examples 'event is applicable'
    end

    context 'when new workflow label is correct' do
      let(:label_names) { ['workflow::complete', 'issue::complete'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when no labels are set' do
      let(:label_names) { [] }

      it 'adds a comment with a quick action adding the correct label' do
        expect(subject).to receive(:add_comment).with('/label ~issue::validation', append_source_link: false)

        subject.process
      end
    end

    context 'when new workflow label needs adding' do
      let(:label_names) { ['workflow::complete'] }

      it 'adds a comment with a quick action adding the correct label' do
        expect(subject).to receive(:add_comment).with('/label ~issue::complete', append_source_link: false)

        subject.process
      end

      context 'when new issue workflow label should be ~issue::planning' do
        let(:label_names) { ['workflow::design'] }

        it 'adds a comment with a quick action adding the correct label' do
          expect(subject).to receive(:add_comment).with('/label ~issue::planning', append_source_link: false)

          subject.process
        end
      end
    end

    context 'when new workflow label needs updating' do
      let(:label_names) { ['workflow::ready for development', 'issue::validation'] }

      it 'adds a comment with a quick action adding the correct label' do
        expect(subject).to receive(:add_comment).with('/label ~issue::development', append_source_link: false)

        subject.process
      end
    end

    context 'when an unrecognised legacy workflow label is set' do
      let(:label_names) { ['workflow::non-existent'] }

      it 'adds a comment with a quick action adding the correct label' do
        expect(subject).to receive(:add_comment).with('/label ~issue::validation', append_source_link: false)

        subject.process
      end
    end
  end
end
