# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/delivery/security_sync_approve'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::SecuritySyncApprove do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        from_gitlab_org?: true,
        from_security_fork?: true,
        target_branch_is_main_or_master?: true
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', [
    "merge_request.unapproval",
    "merge_request.open",
    "merge_request.reopen",
    "merge_request.update",
    "merge_request.unapproved"
  ]

  describe '#applicable?' do
    context 'when event is a merge request from security:master to canonical:master' do
      before do
        allow(event).to receive(:source_branch_is?).with('master').and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when source project is not a security fork' do
      before do
        allow(event).to receive(:from_security_fork?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event source branch is not master' do
      before do
        allow(event).to receive(:source_branch_is?).with('master').and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event target branch is not master' do
      before do
        allow(event).to receive(:source_branch_is?).with('master').and_return(true)
        allow(event).to receive(:target_branch_is_main_or_master?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'approves the merge request' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          This merge request is a security->canonical sync.

          All the changes included have already been reviewed by the releavant maintainers and AppSec on our security mirror.

          /approve
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
