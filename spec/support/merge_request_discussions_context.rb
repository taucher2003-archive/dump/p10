# frozen_string_literal: true

RSpec.shared_context 'with merge request discussions' do
  include_context 'with merge request notes'

  let(:discussions_path) { "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions" }
  let(:specific_discussion_id) { 'merge_request_discussion_id' }
  let(:specific_discussion_path) { "#{discussions_path}/#{specific_discussion_id}" }

  let(:merge_request_discussions) do
    [id: specific_discussion_id, notes: merge_request_notes]
  end

  let(:thread_mark) do
    subject.__send__(:unique_thread).__send__(:hidden_comment)
  end
end
