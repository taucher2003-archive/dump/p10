# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/markdown_table'
require_relative '../../triage/triage/result'

RSpec.describe Triage::MarkdownTable do
  let(:result) { Triage::Result.new }

  subject { described_class.new(result) }

  describe '#markdown' do
    context 'when result includes only a single error' do
      before do
        result.errors << 'Please add a type label'
      end

      it 'returns a markdown table with a single error' do
        expected_table = <<~MARKDOWN.chomp
          | | 1 Error |
          | -------- | -------- |
          | :x: | Please add a type label |
        MARKDOWN

        expect(subject.markdown).to eq(expected_table)
      end
    end

    context 'when result includes only a single warning' do
      before do
        result.warnings << 'Please add a subtype label'
      end

      it 'returns a markdown table with a single warning' do
        expected_table = <<~MARKDOWN.chomp
          | | 1 Warning |
          | -------- | -------- |
          | :warning: | Please add a subtype label |
        MARKDOWN

        expect(subject.markdown).to eq(expected_table)
      end
    end

    context 'when result includes multiple errors' do
      before do
        result.errors << 'Please add a subtype label'
        result.errors << 'Please set a milestone'
      end

      it 'returns a markdown table with multiple errors' do
        expected_table = <<~MARKDOWN.chomp
          | | 2 Errors |
          | -------- | -------- |
          | :x: | Please add a subtype label |
          | :x: | Please set a milestone |
        MARKDOWN

        expect(subject.markdown).to eq(expected_table)
      end
    end

    context 'when result includes both an error and a warning' do
      before do
        result.errors << 'Please add a type label'
        result.warnings << 'Please add a subtype label'
      end

      it 'returns a markdown table with an error and a warning' do
        expected_table = <<~MARKDOWN.chomp
          | | 1 Error, 1 Warning |
          | -------- | -------- |
          | :x: | Please add a type label |
          | :warning: | Please add a subtype label |
        MARKDOWN

        expect(subject.markdown).to eq(expected_table)
      end
    end
  end
end
