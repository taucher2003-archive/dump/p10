# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/quick_win/message_formatter'
require_relative '../../../triage/triage'
require_relative '../../../triage/triage/label_event_finder'

RSpec.describe QuickWin::MessageFormatter do
  let(:test_class) do
    Class.new do
      include QuickWin::MessageFormatter
      include Triage::LabelImplementationPlan

      attr_reader :resource, :label_event_finder

      def initialize(resource, label_event_finder)
        @resource = resource
        @label_event_finder = label_event_finder
      end

      def has_empty_or_no_implementation_plan?(_description)
        false
      end
    end
  end

  let(:resource) { { author: { username: 'author_user' } } }
  let(:label_event_finder) { instance_double(Triage::LabelEventFinder, label_added_username: 'added_by_user') }
  let(:test_instance) { test_class.new(resource, label_event_finder) }

  describe '#quick_win_validation_message' do
    it 'includes the correct mentions' do
      message = test_instance.quick_win_validation_message

      expect(message).to include('@added_by_user, @author_user')
    end

    context 'when implementation plan is not empty' do
      it 'includes the age message' do
        message = test_instance.quick_win_validation_message

        expect(message).to include('it has been over a year since the ~"quick win" label was added')
      end
    end

    context 'when implementation plan is empty' do
      it 'includes the criteria message when implementation plan is empty' do
        allow(test_instance).to receive(:has_empty_or_no_implementation_plan?).and_return(true)

        message = test_instance.quick_win_validation_message

        expect(message).to include('it appears this issue does not meet all of the required criteria')
      end

      it 'includes the unlabel command' do
        message = test_instance.quick_win_validation_message

        expect(message).to include('/unlabel ~"quick win"')
      end

      it 'includes the label command' do
        message = test_instance.quick_win_validation_message

        expect(message).to include('/label ~"automation:quick-win-removed"')
      end
    end
  end

  describe '#mention_users' do
    context 'when author is the same as added_by_username' do
      let(:label_event_finder) { instance_double(Triage::LabelEventFinder, label_added_username: 'author_user') }

      it 'returns only the author' do
        expect(test_instance.send(:mention_users)).to eq(['author_user'])
      end
    end

    context 'when added_by_username is blank' do
      let(:label_event_finder) { instance_double(Triage::LabelEventFinder, label_added_username: '') }

      it 'returns only the author' do
        expect(test_instance.send(:mention_users)).to eq(['author_user'])
      end
    end

    context 'when author and added_by_username are different' do
      it 'returns both usernames' do
        expect(test_instance.send(:mention_users)).to eq(%w[added_by_user author_user])
      end
    end
  end

  describe '#author' do
    it 'returns the author username from the resource' do
      expect(test_instance.send(:author)).to eq('author_user')
    end
  end

  describe '#added_by_username' do
    it 'returns the username from label_event_finder' do
      expect(test_instance.send(:label_added_username)).to eq('added_by_user')
    end
  end
end
