# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/resources/milestone'

RSpec.describe Triage::Milestone do
  around do |example|
    Timecop.freeze(2022, 8, 1) { example.run }
  end

  let(:milestone_attr) do
    { 'id' => '456',
      'title' => 'group milestone',
      'start_date' => start_date,
      'due_date' => due_date }
  end

  let(:date_today)  { Date.today }
  let(:today)       { date_today.to_s }
  let(:yesterday)   { (date_today - 1).to_s }
  let(:tomorrow)    { (date_today + 1).to_s }
  let(:in_30_days)  { (date_today + 30).to_s }
  let(:start_date)  { today }
  let(:due_date)    { tomorrow }

  subject { described_class.new(milestone_attr) }

  RSpec.shared_examples 'is not in progress' do
    it 'returns false' do
      expect(subject.in_progress?).to be false
    end
  end

  RSpec.shared_examples 'is in progress' do
    it 'returns true' do
      expect(subject.in_progress?).to be true
    end
  end

  describe '#in_progress?' do
    context 'with contextual milestone when start date is today, due_date is in the future' do
      it_behaves_like 'is in progress'
    end

    context 'when start_date is in the future' do
      let(:start_date) { tomorrow }

      it_behaves_like 'is not in progress'
    end

    context 'when due_date is in the past' do
      let(:due_date) { yesterday }

      it_behaves_like 'is not in progress'
    end

    context 'when start_date is in the past, due_date is in the future' do
      let(:start_date) { yesterday }

      it_behaves_like 'is in progress'
    end

    context 'when there is no start_date' do
      let(:start_date) { nil }

      it_behaves_like 'is not in progress'
    end

    context 'when there is no due_date' do
      let(:due_date) { nil }

      it_behaves_like 'is in progress'
    end
  end

  describe '#starts_within_the_next_days?' do
    context 'when milestone start_date is in 30 days' do
      let(:start_date) { in_30_days }

      it 'starts within 40 days' do
        expect(subject.starts_within_the_next_days?(40)).to be true
      end

      it 'does not start within 20 days' do
        expect(subject.starts_within_the_next_days?(20)).to be false
      end
    end
  end
end
