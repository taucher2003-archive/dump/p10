# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/resources/merge_request'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::MergeRequest do
  include_context 'with event', Triage::PipelineEvent

  let(:project_id) { 222 }
  let(:iid) { 123 }
  let(:title) { 'MR title' }
  let(:web_url) { 'web_url' }

  let(:labels) { %w[foo bar] }
  let(:author) { { 'id' => 42 } }
  let(:reviewer) { { 'id' => 43 } }
  let(:detailed_merge_status) { 'mergeable' }
  let(:merge_when_pipeline_succeeds) { false }
  let(:mr_attrs) do
    {
      'iid' => iid,
      'target_project_id' => 456,
      'title' => title,
      'web_url' => web_url,
      'labels' => labels,
      'author' => author,
      'detailed_merge_status' => detailed_merge_status,
      'merge_when_pipeline_succeeds' => merge_when_pipeline_succeeds
    }
  end

  subject { described_class.new(mr_attrs) }

  describe '#iid' do
    it 'returns the MR iid' do
      expect(subject.iid).to eq(iid)
    end
  end

  describe '#title' do
    it 'returns the MR title' do
      expect(subject.title).to eq(title)
    end
  end

  describe '#web_url' do
    it 'returns the MR web_url' do
      expect(subject.web_url).to eq(web_url)
    end

    context 'when attributes has a url key' do
      context 'without web_url key' do
        it 'returns the url value' do
          expect(described_class.new(url: 'url').web_url).to eq('url')
        end
      end

      context 'with web_url key' do
        it 'returns the web_url value' do
          expect(described_class.new(url: 'url', web_url: 'web_url').web_url).to eq('web_url')
        end
      end
    end
  end

  describe '#detailed_merge_status' do
    it 'returns the detailed merge status' do
      expect(subject.detailed_merge_status).to eq(detailed_merge_status)
    end
  end

  describe '#ready_and_approved?' do
    context 'when detailed_merge_status is mergeable' do
      it 'returns true' do
        expect(subject.ready_and_approved?).to eq(true)
      end
    end

    Triage::NOT_MERGEABLE_STATUSES.each do |status|
      context "when detailed_merge_status is #{status}" do
        let(:detailed_merge_status) { status }

        it 'returns false' do
          expect(subject.ready_and_approved?).to eq(false)
        end
      end
    end
  end

  context 'when it is built from event' do
    let(:mr_attrs) do
      {
        'iid' => iid,
        'target_project_id' => 456,
        'title' => title,
        'web_url' => web_url,
        'detailed_merge_status' => detailed_merge_status
      }
    end

    let(:mr_attrs_from_api) do
      mr_attrs.merge({
        'author' => author,
        'assignees' => [author],
        'reviewers' => [reviewer],
        'labels' => labels,
        'upvotes' => 100, # Test if this can be ignored
        'detailed_merge_status' => detailed_merge_status
      })
    end

    subject { described_class.build_from_event(event, mr_attrs) }

    before do
      stub_api_request(
        path: "/projects/#{mr_attrs['target_project_id']}/merge_requests/#{mr_attrs['iid']}",
        response_body: mr_attrs_from_api)
    end

    describe '#labels' do
      it 'populates the data from API and returns the labels' do
        expect(subject.labels).to eq(labels)
      end
    end

    describe '#author' do
      it 'populates the data from API and returns the author' do
        expect(subject.author).to eq(author)
      end
    end

    describe '#assignees' do
      it 'populates the data from API and returns the author' do
        expect(subject.assignees).to contain_exactly(author)
      end
    end

    describe '#reviewers' do
      it 'populates the data from API and returns the author' do
        expect(subject.reviewers).to contain_exactly(reviewer)
      end
    end
  end

  context 'when it is built from API' do
    subject { described_class.build_from_api(mr_attrs) }

    describe '#labels' do
      it 'returns the MR labels' do
        expect(subject.labels).to eq(labels)
      end
    end

    describe '#author' do
      it 'returns the MR author' do
        expect(subject.author).to eq(author)
      end
    end

    describe '#merge_when_pipeline_succeeds' do
      it 'returns the merge_when_pipeline_succeeds boolean' do
        expect(subject.merge_when_pipeline_succeeds).to eq(merge_when_pipeline_succeeds)
      end
    end
  end
end
