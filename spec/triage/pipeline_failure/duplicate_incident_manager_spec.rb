# frozen_string_literal: true

require_relative '../../../triage/triage/pipeline_failure/duplicate_incident_manager'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../triage/resources/ci_job'

RSpec.describe Triage::PipelineFailure::DuplicateIncidentManager do
  def build_incident_double(title:, iid:, closed_as_duplicate_link: nil, description: 'description', labels: [])
    instance_double(
      'Incident',
      iid: iid,
      title: title,
      description: description,
      state: 'closed',
      labels: labels,
      _links: { 'self' => 'self', 'closed_as_duplicate_of' => closed_as_duplicate_link }
    )
  end

  let(:config) do
    instance_double(
      Triage::PipelineFailure::Config::MasterBranch,
      incident_project_id: '1',
      escalate_on_stale?: escalate_on_stale
    )
  end

  let(:escalate_on_stale) { false }

  let(:previous_incidents) { [] }

  let(:failed_job_names) { %w[jest cypress] }

  let(:pytest_incident_double) do
    build_incident_double(
      title: 'broken pipeline with pytest',
      description: '- [pytest](pytest_job_url) **Job ID**: `2` (retry with `@gitlab-bot retry_job 2`)',
      iid: '2'
    )
  end

  let(:rspec_jest_incident_double) do
    build_incident_double(
      title: 'broken pipeline with rspec, jest',
      description: "- [rspec](rspec_job_url) \n- [jest](jest_job_url)",
      iid: '3'
    )
  end

  let(:duplicate_source_double) do
    build_incident_double(
      title: 'broken pipeline with rspec, jest',
      description: "- [jest](jest_job_url) **Job ID**: `1` \n- [rspec](rspec_job_url) **Job ID**: `2`",
      iid: '1',
      labels: incident_labels
    )
  end

  let(:incident_labels) { [] }

  let(:event) do
    instance_double(Triage::PipelineEvent, sha: 'abc')
  end

  let(:canonical_incident_double) do
    build_incident_double(
      title: '`gitlab-org/gitlab` broken pipeline',
      iid: '12',
      description: 'broken pipeline on abc'
    )
  end

  subject(:duplicate_incident_manager) { described_class.new(config: config, failed_job_names: failed_job_names) }

  before do
    allow(Triage.api_client).to receive(:issues).and_return(previous_incidents)
  end

  describe '#has_duplicate?' do
    context 'with no previous incidents' do
      it 'returns false' do
        expect(duplicate_incident_manager.has_duplicate?).to be false
      end
    end

    context 'with previous incidents not matching the new incident' do
      let(:previous_incidents) { [pytest_incident_double] }

      it 'returns false' do
        expect(duplicate_incident_manager.has_duplicate?).to be false
      end
    end

    context 'with previous incident showing 0 failed jobs' do
      let(:empty_jobs_incident_double) do
        build_incident_double(title: 'broken pipeline with', iid: '4', description: '**Failed jobs (0):**')
      end

      let(:previous_incidents) { [empty_jobs_incident_double] }

      context 'with job_names being empty' do
        let(:failed_job_names) { [] }

        it 'returns true' do
          expect(duplicate_incident_manager.has_duplicate?).to be true
        end
      end

      context 'with job_names not empty' do
        let(:failed_job_names) { %w[rspec] }

        it 'returns false' do
          expect(duplicate_incident_manager.has_duplicate?).to be false
        end
      end
    end

    context 'with the current and previous incidents have at least one common failed job' do
      let(:previous_incidents) { [rspec_jest_incident_double] }
      let(:failed_job_names)   { %w[jest cypress playwright] }

      it 'returns true' do
        expect(duplicate_incident_manager.has_duplicate?).to be true
      end
    end
  end

  describe '#post_warning?' do
    context 'when config escalate_on_stale is false' do
      it 'returns false' do
        expect(duplicate_incident_manager.post_warning?).to be false
      end
    end

    context 'when config escalate_on_stale is true' do
      let(:escalate_on_stale) { true }

      context 'with no duplicate source incident' do
        it 'returns false' do
          expect(duplicate_incident_manager.post_warning?).to be false
        end
      end

      context 'with a previous duplicate incident' do
        let(:previous_incidents) { [rspec_jest_incident_double] }

        let(:unique_comment_double) do
          instance_double(
            Triage::UniqueComment,
            no_previous_comment?: no_previous_comment,
            wrap: "<!-- triage-serverless #{described_class.name} -->"
          )
        end

        before do
          allow(Triage::UniqueComment).to receive(:new).and_return(unique_comment_double)
        end

        context 'when there was no previous comment' do
          let(:no_previous_comment) { true }

          it 'returns true' do
            expect(duplicate_incident_manager.post_warning?).to be true
          end
        end

        context 'when there was a previous comment' do
          let(:no_previous_comment) { false }

          it 'returns false' do
            expect(duplicate_incident_manager.post_warning?).to be false
          end
        end
      end
    end
  end

  describe 'duplicate_source_incident' do
    context 'with no previous incident' do
      it 'returns nil' do
        expect(duplicate_incident_manager.duplicate_source_incident).to be nil
      end
    end

    context 'with previous incident NOT matching the failed test names' do
      let(:previous_incidents) { [pytest_incident_double] }

      it 'returns nil' do
        expect(duplicate_incident_manager.duplicate_source_incident).to be nil
      end
    end

    context 'with previous incident matching the failed test names' do
      let(:previous_incidents) { [rspec_jest_incident_double] }

      context 'when the previous incident not linked to any other incident' do
        it 'returns the previous incident' do
          expect(duplicate_incident_manager.duplicate_source_incident).to eq rspec_jest_incident_double
        end
      end

      context 'when the previous duplicate incident closed as a duplicate of another' do
        let(:duplicate_rspec_jest_incident_double) do
          build_incident_double(
            title: 'broken pipeline with rspec, jest',
            iid: '2',
            closed_as_duplicate_link: '/projects/1/issues/1'
          )
        end

        let(:previous_incidents) { [duplicate_source_double, duplicate_rspec_jest_incident_double] }

        before do
          allow(Triage.api_client).to receive(:get).with('/projects/1/issues/1').and_return(duplicate_source_double)
        end

        it 'returns the duplicate source incident' do
          expect(duplicate_incident_manager.duplicate_source_incident).to eq duplicate_source_double
        end
      end
    end
  end

  describe 'warning_comment_body' do
    let(:previous_incidents) { [duplicate_source_double] }

    context 'when label includes escalation:: label' do
      let(:incident_labels) { ['escalation::skipped'] }

      it 'returns duplicate incident warning without starting escalation process' do
        expect(duplicate_incident_manager.warning_comment_body).to eq(
          <<~MARKDOWN.chomp
          <!-- triage-serverless DuplicateIncidentManager -->
          🚨 Multiple duplicate incidents detected!
          MARKDOWN
        )
      end
    end

    context 'when label does not include any escalation:: label' do
      let(:incident_labels) { ['master:broken'] }

      before do
        allow(duplicate_incident_manager).to receive(:today_is_weekend?).and_return(today_is_weekend)
      end

      context 'when today is a weekend' do
        let(:today_is_weekend) { true }

        it 'returns the escalation warning for weekends' do
          expect(duplicate_incident_manager.warning_comment_body).to eq(
            <<~MARKDOWN.chomp
            <!-- triage-serverless DuplicateIncidentManager -->
            🚨 Multiple duplicate incidents detected!

            Automated escalation is not available on weekends and holidays.
            If immediate attention is required, please manually escalate to `#dev-escalaton` following [this handbook page](https://handbook.gitlab.com/handbook/engineering/workflow/#escalation-on-weekends-and-holidays).
            /label ~escalation::needed
            MARKDOWN
          )
        end
      end

      context 'when today is a weekday' do
        let(:today_is_weekend) { false }

        it 'returns the escalation warning' do
          expect(duplicate_incident_manager.warning_comment_body).to eq(
            <<~MARKDOWN.chomp
            <!-- triage-serverless DuplicateIncidentManager -->
            🚨 Multiple duplicate incidents detected!

            This incident will be escalated to `#dev-escalaton` in 4 hours of inactivity.
            /label ~escalation::needed
            MARKDOWN
          )
        end
      end
    end
  end

  describe '#canonical_incident_triggered_by_same_commit?' do
    context 'with no previous incidents' do
      it 'returns false' do
        expect(duplicate_incident_manager.canonical_incident_triggered_by_same_commit?(event)).to be false
      end
    end

    context 'with previous canonical incident by same commit' do
      let(:previous_incidents) { [canonical_incident_double] }

      it 'returns true' do
        expect(duplicate_incident_manager.canonical_incident_triggered_by_same_commit?(event)).to be true
      end
    end
  end
end
