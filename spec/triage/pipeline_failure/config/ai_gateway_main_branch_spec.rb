# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/ai_gateway_main_branch'

RSpec.describe Triage::PipelineFailure::Config::AiGatewayMainBranch do
  let(:project_path_with_namespace) { 'gitlab-org/modelops/applied-ml/code-suggestions/ai-assist' }
  let(:ref) { 'main' }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      ref: ref)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when project_path_with_namespace is not "gitlab-org/modelops/applied-ml/code-suggestions/ai-assist"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when ref is not "main"' do
      let(:ref) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channel' do
      expect(subject.default_slack_channels).to eq(%w[g_ai_framework g_mlops-alerts ai-infrastructure])
    end
  end

  describe '#auto_triage?' do
    it 'returns true' do
      expect(subject.auto_triage?).to be_truthy
    end
  end
end
