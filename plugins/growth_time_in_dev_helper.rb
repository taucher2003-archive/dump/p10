# frozen_string_literal: true

require_relative '../lib/growth_time_in_dev_helper'

Gitlab::Triage::Resource::Context.include GrowthTimeInDevHelper
