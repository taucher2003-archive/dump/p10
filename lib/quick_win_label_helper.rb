# frozen_string_literal: true

require_relative '../triage/triage/quick_win/label_validator'
require_relative '../triage/triage/quick_win/message_formatter'

module QuickWinLabelHelper
  include QuickWin::LabelValidator
  include QuickWin::MessageFormatter
end
