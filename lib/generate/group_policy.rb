# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../hierarchy/group'

module Generate
  module GroupPolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')
      erb.filename = options.template

      FileUtils.rm_rf("#{destination}/#{template_name}")
      raise "#{destination}/#{template_name} is not empty" if Dir.glob("#{destination}/#{template_name}/**/*").any?

      FileUtils.mkdir_p("#{destination}/#{template_name}")

      Hierarchy::Group.all.each do |group|
        next if options.only && !options.only.include?(group.key)
        # p group.definition
        next if group.definition.dig('triage_ops_config', 'ignore_templates')&.include?(options.template)

        raise "Group #{group.key} not found!" if group.nil?

        generated_template_path = "#{destination}/#{template_name}/#{group.key}.yml"
        puts "Generating #{generated_template_path}"

        File.write(
          generated_template_path,
          erb.result_with_hash(
            group: group
          )
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
