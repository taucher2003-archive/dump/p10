# frozen_string_literal: true

require 'google/apis/sheets_v4'
require 'googleauth'

# Interface for Google Sheets (wrapping the Google sheet API)
class GoogleSheetsHelper
  def initialize(sheet_id, sheet_name, google_service_account_credentials_path)
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: File.open(google_service_account_credentials_path),
      scope: 'https://www.googleapis.com/auth/drive'
    )
    @sheet_name = sheet_name
    @sheet_id = sheet_id
  end

  def read
    rows = @service.get_spreadsheet_values(@sheet_id, @sheet_name).values
    headers = rows.shift

    rows.map do |row|
      headers.zip(row).to_h
    end
  end
end
