# frozen_string_literal: true

require_relative 'devops_labels'
require_relative 'team_member_helper'
require_relative 'constants/labels'

module UntriagedHelper
  include DevopsLabels::Context
  include TeamMemberHelper

  TYPE_LABELS = Labels::TYPE_LABELS
  SPECIAL_ISSUE_LABELS = Labels::SPECIAL_ISSUE_LABELS
  TYPE_IGNORE_LABEL = Labels::TYPE_IGNORE_LABEL

  def distribute_items(list_items, triagers)
    puts "Potential triagers: #{triagers.inspect}"

    return {} if triagers.empty?

    triagers.shuffle
      .zip(list_items.each_slice((list_items.size.to_f / triagers.size).ceil))
      .sort { |(triager_a, _), (triager_b, _)| triager_a <=> triager_b }
      .to_h
      .compact
  end

  def distribute_and_display_items_per_triager(list_items, triagers)
    max_items_per_triager = 4
    items_per_triagers = distribute_items(list_items.last(triagers.count * max_items_per_triager), triagers)

    text = items_per_triagers.each_with_object([]) do |(triager, items), text|
      text << "`#{triager}`\n\n#{items.join("\n")}"
    end.join("\n\n")

    remaining = list_items.count - (triagers.size * max_items_per_triager)
    if remaining.positive?
      text += "\n\n" unless text.empty?
      text += "`unassigned` - Got time? Take a couple of these.\n\n#{list_items.first(remaining).join("\n")}"
    end

    text + triager_assignment_markdown(items_per_triagers)
  end

  def triager_assignment_markdown(distributed_items)
    valid_triagers = distributed_items.keys.reject { |triager| triager == 'unassigned' }

    valid_triagers.any? ? "\n\n/assign #{valid_triagers.join(' ')}" : ''
  end

  def merge_requests_from_line_items(items)
    items.lines(chomp: true).map do |item|
      author, url, *labels = item.split(',')
      { author: author, url: url, labels: labels }
    end
  end

  def markdown_table_rows(merge_requests_by_author)
    merge_requests_by_author.map.with_index do |(author, merge_requests), index|
      "| #{index + 1}: #{author} | <ol>#{merge_requests.map { |mr| "<li>#{mr[:url]}: #{mr[:labels].join(' ')}</li>" }.join}</ol> |"
    end
  end

  def untriaged?
    !triaged?
  end

  def triaged?
    special_issue? || has_type_ignore_label? ||
      (
        has_type_label? && has_group_label?
      )
  end

  def special_issue?
    special_issue_labels.any?
  end

  def has_type_label?
    !type_label.nil?
  end

  def special_issue_labels
    (label_names & SPECIAL_ISSUE_LABELS)
  end

  def type_label
    (label_names & TYPE_LABELS).first
  end

  def has_type_ignore_label?
    label_names.include? TYPE_IGNORE_LABEL
  end
end
