# frozen_string_literal: true

require 'yaml'

require_relative './people_set'
require_relative './group'
require_relative './section'

module Hierarchy
  class Stage < PeopleSet
    def self.raw_data
      # No memoization as it's already cached in MiniCache
      WwwGitLabCom.stages
    end

    def section
      @section ||= Hierarchy::Section.new(definition['section']) if Hierarchy::Section.exist?(definition['section'])
    end

    def groups
      @groups ||= definition['groups'].map { |group| Hierarchy::Group.new(group) }
    end

    def to_h(fields = nil)
      super.merge(
        groups: groups
      ).compact
    end

    def allowed_participants
      @allowed_participants ||= groups.reduce({}) do |acc, group|
        acc.merge(group.allowed_participants) { |k, a, b| (a + b).uniq }
      end.compact
    end
  end
end
