# frozen_string_literal: true

require_relative '../constants/labels'
require_relative '../team_member_helper'

module CurrentGroupHelper
  include TeamMemberHelper

  QUALITY_DEPARTMENT_REGEX = /quality department/i
  SOFTWARE_ENGINEER_IN_TEST_REGEX = /software\s+engineer\s+in\s+test/i

  def current_group_em
    return unless current_group

    manager_scope = label_names.include?(Labels::FRONTEND_LABEL) ? :frontend : :backend
    [current_group.engineering_managers(manager_scope).first, current_group.engineering_managers(:generic).first, current_group.engineering_managers(:fullstack).first].reject(&:blank?).first
  end

  def current_group_set
    return unless current_group

    parse_role_and_specialty_to_select_group_member(QUALITY_DEPARTMENT_REGEX, SOFTWARE_ENGINEER_IN_TEST_REGEX)
  end

  def current_group_pm
    return unless current_group

    current_group.product_managers.first
  end

  private

  def parse_role_and_specialty_to_select_group_member(department_regex, role_regex)
    group_regex = Regexp.new(current_group.name.downcase)
    parsed_from_specialty =
      select_team_members_by_department_specialty_role(department_regex,
        group_regex,
        role_regex,
        include_ooo: true).first

    # many team members don't populate the specialty field, but instead put their group info in the role field.
    parsed_from_role =
      select_team_members_by_department_specialty_role(department_regex,
        nil,
        /#{role_regex.source}.*#{group_regex}/i,
        include_ooo: true).first

    parsed_from_specialty || parsed_from_role
  end
end
