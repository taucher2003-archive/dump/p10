# frozen_string_literal: true

require_relative 'devops_labels'

module PriorityMappingHelper
  include DevopsLabels::Context

  SEVERITY_REGEX = /severity::[1-4]/
  PRIORITY_REGEX = /priority::[1-4]/

  SEVERITY_PRIORITY_MAP = {
    "severity::1" => "priority::1",
    "severity::2" => "priority::2"
  }.freeze

  MERGE_REQUEST_LABEL = "merge requests"
  UX_LABEL = "UX"

  MR_SEVERITY_PRIORITY_MAP = {
    "severity::1" => "priority::1",
    "severity::2" => "priority::1",
    "severity::3" => "priority::2",
    "severity::4" => "priority::3"
  }.freeze

  MR_PRIORITY_OVERRIDE_COMMENT = <<~STR
    This ~"bug" affects ~"merge requests" ~"UX", Merge Request experience is the core of our product and we need help prioritizing according to our learnings from addressing Transient Issues. Prioritization has been automatically updated per our [merge request experience prioritization guidelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/issue-triage/#merge-requests-experience-prioritization)
  STR

  def set_priority_for_bug
    priority_label = determine_priority_label
    override_comment = override_comment(priority_label)

    [].tap do |priority_comment|
      priority_comment << override_comment if override_comment
      priority_comment << "/label #{priority_label}" if priority_label
    end.join("\n")
  end

  def has_severity?
    label_names.grep(SEVERITY_REGEX).any?
  end

  def has_no_severity?
    !has_severity?
  end

  def has_priority?
    label_names.grep(PRIORITY_REGEX).any?
  end

  def has_no_priority?
    !has_priority?
  end

  private

  def determine_priority_label
    priority_label = nil

    if has_severity?
      if has_no_priority?
        priority_label = if merge_request_functionality_related?
                           MR_SEVERITY_PRIORITY_MAP[severity]
                         else
                           SEVERITY_PRIORITY_MAP[severity]
                         end
      elsif merge_request_functionality_related? && priority_override?(MR_SEVERITY_PRIORITY_MAP[severity])
        priority_label = MR_SEVERITY_PRIORITY_MAP[severity]
      end
    end

    "~\"#{priority_label}\"" if priority_label
  end

  def priority_override?(new_priority)
    new_priority && priority && (new_priority != priority)
  end

  def override_comment(new_priority)
    MR_PRIORITY_OVERRIDE_COMMENT if priority_override?(new_priority) && merge_request_functionality_related?
  end

  def severity
    label_names.grep(SEVERITY_REGEX).first
  end

  def priority
    label_names.grep(PRIORITY_REGEX).first
  end

  def merge_request_functionality_related?
    label_names.grep(MERGE_REQUEST_LABEL).any? && label_names.grep(UX_LABEL).any?
  end
end
