stages:
  - dry-run
  - schedules-sync
  - single-run
  - test
  - pre-hygiene
  - hygiene
  - report
  - close-reports
  - one-off
  - build
  - secrets
  - deploy
  - health-check

workflow:
  name: '$PIPELINE_NAME'
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
      variables:
        PIPELINE_NAME: '$CI_MERGE_REQUEST_EVENT_TYPE MR pipeline'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For dry-running a schedule with `dry-run:schedule`
    - if: '$CI_PIPELINE_SOURCE == "trigger"'

default:
  image: "registry.gitlab.com/gitlab-org/gitlab-build-images/${BUILD_OS}-${OS_VERSION}-ruby-${RUBY_VERSION}"
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile.lock
      prefix: "ruby-gems"
    paths:
      - vendor/ruby/

variables:
  BUNDLE_FROZEN: "true"
  BUNDLE_PATH__SYSTEM: 'false'
  BUNDLE_INSTALL_FLAGS: "--jobs=$(nproc) --retry=3 --quiet"
  TRIAGE_WEB_IMAGE_PATH: "${CI_REGISTRY}/${CI_PROJECT_PATH}/web:${CI_COMMIT_REF_SLUG}"
  BUILD_OS: "debian"
  OS_VERSION: "bookworm"
  RUBY_VERSION: "3.2.4"
  DOCKER_VERSION: "26.1.4"
  SIMPLECOV: "true"
  JUNIT_RESULT_FILE: "rspec/junit_rspec.xml"
  COVERAGE_FILE: "coverage/coverage.xml"

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

include:
  - local: .gitlab/ci/*.yml
  - local: .gitlab/ci/generated/*.yml
  - component: ${CI_SERVER_FQDN}/gitlab-org/components/danger-review/danger-review@2.0.0
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

semgrep-sast:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

secret_detection:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

gemnasium-dependency_scanning:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
