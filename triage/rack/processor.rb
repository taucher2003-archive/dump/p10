# frozen_string_literal: true

require 'rack'
require_relative '../job/processor_job'

module Triage
  module Rack
    class Processor
      def call(env)
        begin
          payload = JSON.parse(env['rack.input'].read)
        rescue JSON::ParserError => e
          return ::Rack::Response.new([JSON.dump({ status: :error, error: e.class, message: e.message })], 400).finish
        end

        ::Triage::ProcessorJob.perform_async(payload)
        ::Rack::Response.new([JSON.dump({ status: :ok })]).finish
      end
    end
  end
end
