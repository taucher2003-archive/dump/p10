# frozen_string_literal: true

module Triage
  module PipelineFailure
    class SpecDurationData
      attr_reader :spec_file, :expected_duration, :actual_duration

      def initialize(spec_file:, expected_duration:, actual_duration:)
        @spec_file = spec_file
        @expected_duration = expected_duration&.to_f
        @actual_duration = actual_duration&.to_f
      end

      def markdown_table_row
        "|#{spec_file}|#{readable_expected_duration}|#{readable_actual_duration}|#{readable_diff_percentage}|"
      end

      private

      def readable_expected_duration
        readable_duration(expected_duration)
      end

      def readable_actual_duration
        readable_duration(actual_duration)
      end

      def readable_diff_percentage
        return '' if expected_duration.nil? || actual_duration.nil?

        value = (actual_duration - expected_duration) / expected_duration * 100
        positive_sign = value.positive? ? '+' : ''

        "#{positive_sign}#{value.to_i}%"
      end

      def readable_duration(duration_in_seconds)
        return '' unless duration_in_seconds

        minutes = (duration_in_seconds / 60).to_i
        seconds = (duration_in_seconds % 60).round(2)

        min_output = normalize_duration_output(minutes, 'minute')
        sec_output = normalize_duration_output(seconds, 'second')

        "#{min_output} #{sec_output}".strip
      end

      def normalize_duration_output(number, unit)
        if number <= 0
          ""
        elsif number <= 1
          "#{number} #{unit}"
        else
          "#{number} #{unit}s"
        end
      end
    end
  end
end
