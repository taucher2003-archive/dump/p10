# frozen_string_literal: true

require_relative '../triage'
require_relative 'changed_file_list'
require_relative '../../lib/constants/labels'

module Triage
  class NeedMrApprovedLabel
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|\.gitlab/(issue|merge_request)_templates)/}
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def need_mr_approved_label?
      !expedited_mr? &&
        !event.source_branch_is?(UPDATE_GITALY_BRANCH) &&
        !changed_file_list.only_change?(SKIP_WHEN_CHANGES_ONLY_REGEX)
    end

    private

    # We fetch the labels from the API, because we noticed some race conditions where an
    # merge_request.open event would arrive late (~1min after the MR was actually opened),
    # and when checking for the expedited label, we concluded that it wasn't applied on the MR.
    #
    # Because we don't want to ever remove an expedited label, we check the MR labels from the API.
    def expedited_mr?
      merge_request_labels.include?(Labels::PIPELINE_EXPEDITED_LABEL)
    end

    def merge_request_labels
      @merge_request_labels ||=
        merge_request_from_api&.labels || event.label_names
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def merge_request_from_api
      Triage.api_client.merge_request(event.project_id, event.iid)
    rescue Gitlab::Error::InternalServerError
      nil
    end
  end
end
