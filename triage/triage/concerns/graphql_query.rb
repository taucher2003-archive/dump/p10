# frozen_string_literal: true

require 'graphql/client'
require 'graphql/client/http'
require 'httparty'

require_relative '../../triage'

module Triage
  module GraphqlQuery
    GITLAB_API = 'https://gitlab.com/api/graphql'
    USER_AGENT = "GitLab Quality Triage-ops"
    GRAPHQL_CACHE_EXPIRY = 86_400 # 1 day

    def query(query, resource_path: [], variables: {})
      parsed_query = client.parse(query)
      response = client.query(parsed_query, variables: {}, context: {})

      return response.errors.to_h unless response.errors.empty?

      parse_response(response, resource_path)
    end

    def schema
      ::Triage.cache.get_or_set(:graphql_schema, expires_in: GRAPHQL_CACHE_EXPIRY) do
        fetch_schema
      end
    end

    private

    def http_client
      GraphQL::Client::HTTP.new(GITLAB_API) do
        def execute(document:, operation_name: nil, variables: {}, context: {}) # rubocop:disable Lint/NestedMethodDefinition
          body = {}
          body['query'] = document.to_query_string
          body['variables'] = variables if variables.any?
          body['operationName'] = operation_name if operation_name

          response = HTTParty.post(
            uri,
            body: body.to_json,
            headers: {
              'User-Agent' => USER_AGENT,
              'Content-type' => 'application/json',
              'PRIVATE-TOKEN' => context[:token]
            }
          )

          case response&.code
          when 200, 400
            JSON.parse(response.body).merge('extensions' => { 'headers' => response.headers })
          else
            { 'errors' => [{ 'message' => "#{response.code} #{response.message}" }] }
          end
        end
      end
    end

    def fetch_schema
      GraphQL::Client.load_schema(http_client)
    end

    def client
      @client ||= GraphQL::Client.new(schema: schema, execute: http_client).tap { |client| client.allow_dynamic_queries = true }
    end

    def parse_response(response, resource_path)
      resource_path.reduce(response.data) { |data, resource| data&.send(resource) }
    end
  end
end
