# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class FeatureMrSeedingScriptReminder < Processor
    react_to 'merge_request.open', 'merge_request.update'

    REQUIRED_LABELS = %w[feature::addition group::optimize].freeze

    def applicable?
      event.from_gitlab_org_gitlab? &&
        with_required_labels? &&
        !previous_discussion?
    end

    def process
      add_discussion(discussion_body, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor reminds MR author for ~"feature::addition" ~"group::optimize" MRs to create/update seeding scripts.
      TEXT
    end

    private

    def with_required_labels?
      (REQUIRED_LABELS - event.label_names).empty?
    end

    def previous_discussion?
      unique_comment.previous_discussion
    end

    def discussion_body
      message_markdown = <<~MARKDOWN
        :wave: Hi @#{event.resource_author.username},

        Please ensure **data seeding scripts** are created and/or updated by following the [optimize group handbook page](https://handbook.gitlab.com/handbook/engineering/development/dev/plan/optimize/#data-seeding-scripts).
      MARKDOWN

      unique_comment.wrap(message_markdown).strip
    end
  end
end
