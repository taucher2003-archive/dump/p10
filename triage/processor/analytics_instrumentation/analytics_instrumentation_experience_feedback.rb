# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'
require_relative '../../../lib/constants/labels'

module Triage
  class AnalyticsInstrumentationExperienceFeedback < Processor
    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      mr_reviewed_or_approved? &&
        unique_comment.no_previous_comment? &&
        event.not_spam? &&
        !event.maybe_automation_author?
    end

    def process
      add_comment(message.strip, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor asks for feedback about the analytics instrumentation review experience.
      TEXT
    end

    private

    def message
      comment = <<~MARKDOWN.chomp
        Hello @#{event.resource_author.username} :wave:

        The Analytics Instrumentation team is actively working on improving the metrics implementation and event tracking processes.
        We would love to hear about your experience and any feedback you might have!

        To provide your feedback, please use [this Google Form](https://forms.gle/PFUTVPWSQqSJ4PYs5).

        Thanks for your help! :heart:

        ---

        - _Looking for existing metrics data?_ Check out the [Metrics Exploration Dashboard for Group:#{event.group_name}](#{metric_exploration_group_link}).
        - _Need further assistance? Have comments?_
            - Mention `@gitlab-org/analytics-section/analytics-instrumentation/engineers`
            - Reach out in [#g_monitor_analytics_instrumentation](https://gitlab.enterprise.slack.com/archives/CL3A7GFPF) channel on Slack.
      MARKDOWN

      unique_comment.wrap(comment)
    end

    def mr_reviewed_or_approved?
      event.label_names.include?(Labels::ANALYTICS_INSTRUMENTATION_APPROVED_LABEL) ||
        event.label_names.include?(Labels::ANALYTICS_INSTRUMENTATION_REVIEW_PENDING_LABEL)
    end

    def metric_exploration_group_link
      "https://10az.online.tableau.com/#/site/gitlab/views/PDServicePingExplorationDashboard/MetricExplorationbyGroup?Group%20Name=#{event.group_name}&Stage%20Name=#{event.stage_name}"
    end
  end
end
