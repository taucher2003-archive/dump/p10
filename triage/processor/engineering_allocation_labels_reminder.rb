# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../job/engineering_allocation_labels_reminder_job'
require_relative '../triage/engineering_allocation_labels_validator'

module Triage
  class EngineeringAllocationLabelsReminder < Processor
    react_to 'issue.open', 'issue.update'

    ENGINEERING_ALLOCATION_LABEL = 'Engineering Allocation'
    FIVE_MINUTES = 300

    def applicable?
      event.from_gitlab_org? &&
        engineering_allocation_label_added? &&
        validator.label_nudge_needed?
    end

    def comment_cleanup_applicable?
      unique_comment.previous_comment? && validator.issue_has_required_labels?
    end

    def process
      SuckerPunch.logger.info("Scheduling job EngineeringAllocationLabelsReminderJob - Issue URL: #{event.url}")
      EngineeringAllocationLabelsReminderJob.perform_in(FIVE_MINUTES, event)
    end

    def processor_comment_cleanup
      unique_comment.delete_previous_comment
    end

    def documentation
      <<~TEXT
        This processor is triggered when an issue is labeled with `~Engineering Allocation`.
        It schedules EngineeringAllocationLabelsReminderJob to run against this issue in 5 minutes.
      TEXT
    end

    private

    def engineering_allocation_label_added?
      event.added_label_names.include?(ENGINEERING_ALLOCATION_LABEL)
    end

    def validator
      @validator ||= EngineeringAllocationLabelsValidator.new(event)
    end

    def unique_comment
      @unique_comment ||= validator.unique_comment
    end
  end
end
