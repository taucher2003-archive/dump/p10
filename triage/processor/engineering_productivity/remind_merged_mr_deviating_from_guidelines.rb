# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class RemindMergedMrDeviatingFromGuideline < Processor
    HOURS_THRESHOLD_FOR_CREATED_STALE_MR_PIPELINE = 8
    MERGE_RESULTS_REF_REGEX = %r{\Arefs/merge-requests/\d+/merge\z}
    MERGE_TRAIN_REF_REGEX = %r{\Arefs/merge-requests/\d+/train\z}

    react_to 'merge_request.merge'

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.target_branch_is_main_or_master? &&
        !latest_pipeline_is_merge_train? &&
        !latest_pipeline_valid?
    end

    def process
      remind_merged_mr_deviating_from_guidelines
    end

    def documentation
      <<~TEXT
        For merged MR, if the latest pipeline doesn't match all these conditions:
          1. Pipeline is a canonical one (i.e. didn't run in the context of a fork)
          2. Pipeline was created less than 8 hours ago (72 hours for stable branches)
        We remind the MR merger about the "Merging a merge request" process.
      TEXT
    end

    private

    def project_id
      event.project_id
    end

    def latest_pipeline
      @latest_pipeline ||= Triage.api_client.merge_request_pipelines(project_id, event.iid).first
    end

    def latest_pipeline_is_merge_train?
      MERGE_TRAIN_REF_REGEX.match?(latest_pipeline&.ref)
    end

    def latest_pipeline_valid?
      latest_pipeline_is_canonical? &&
        latest_pipeline_is_merge_results? &&
        latest_pipeline_is_recent_enough?
    end

    def latest_pipeline_is_canonical?
      event.with_project_id?(latest_pipeline&.project_id)
    end

    def latest_pipeline_is_merge_results?
      MERGE_RESULTS_REF_REGEX.match?(latest_pipeline&.ref)
    end

    def latest_pipeline_detailed
      @latest_pipeline_detailed ||= Triage.api_client.pipeline(project_id, latest_pipeline&.id)
    end

    def latest_pipeline_is_recent_enough?
      Time.parse(latest_pipeline_detailed.created_at) >= (Time.now - (3600 * HOURS_THRESHOLD_FOR_CREATED_STALE_MR_PIPELINE))
    end

    def remind_merged_mr_deviating_from_guidelines
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username}, did you forget to run a pipeline before you merged this work?

        Based on our [code review process](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request),
        the latest pipeline before merge must match the following conditions:

        1. Pipeline is a canonical one (i.e. didn't run in the context of a fork)
        2. Pipeline is a ["tier-3"](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) one (i.e. the merge request has the ~"pipeline::tier-3" label set)
        3. Pipeline is a [Merged Results](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html) one
        4. Pipeline was created at most #{HOURS_THRESHOLD_FOR_CREATED_STALE_MR_PIPELINE} hours ago (72 hours for stable branches)

        This rule shouldn't be ignored as this can lead to a broken `master`. Please consider replying to this comment for transparency.
        /label ~"pipeline:too-old"
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
