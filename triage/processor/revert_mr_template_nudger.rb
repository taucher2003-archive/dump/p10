# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/constants/labels'

module Triage
  class RevertMrTemplateNudger < Processor
    react_to 'merge_request.open'

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.revert_mr? &&
        missing_expedited_label?
    end

    def process
      add_comment(revert_mr_template_nudger_comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
        Reminds the MR creator to use the revert MR template for revert MRs
        if ~pipeline::expedited label is missing, as it will set appropriate
        labels to speed up the revert.
      TEXT
    end

    private

    def missing_expedited_label?
      !event.label_names.include?(Labels::PIPELINE_EXPEDITED_LABEL)
    end

    def revert_mr_template_nudger_comment
      <<~MARKDOWN.chomp
        :wave: @#{event.resource_author.username},

        To speed up your revert merge request, please consider using the [revert merge request template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Revert%20To%20Resolve%20Incident.md). Adding the appropriate labels for resolving ~"master:broken" **before** the merge request is created will skip several CI/CD jobs.

        For this merge request, if this is for resolving ~"master:broken" you can add the appropriate labels present in the merge request template, and trigger a new pipeline. It will be faster :rocket:.

        [See the docs](https://docs.gitlab.com/ee/development/pipelines#revert-mrs).
      MARKDOWN
    end
  end
end
