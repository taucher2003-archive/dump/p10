# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../triage/unique_comment'
require_relative '../../triage/changed_file_list'
require_relative '../../triage/concerns/codeowners'

module Triage
  class GrowthAffectingNotifier < CommunityProcessor
    include Codeowners

    GROWTH_ALERT_FILES = [
      'app/views/admin/application_settings/_account_and_limit.html.haml',
      'app/models/user.rb',
      'ee/app/models/ee/user.rb',
      'app/controllers/admin/users_controller.rb'
    ].freeze

    CODEOWNERS_ENTRY = '@gitlab-org/growth/engineers'
    CODEOWNERS_FILE = 'https://gitlab.com/gitlab-org/gitlab/-/raw/master/.gitlab/CODEOWNERS'

    NOTIFICATION_RECIPIENTS = %w[
      @kniechajewicz
    ].freeze

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        growth_affecting_change? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor notifies Growth DRIs to review merge request when changes affect certain files that could affect user growth.
      TEXT
    end

    private

    def review_request_comment
      comment = <<~MARKDOWN.chomp
        #{NOTIFICATION_RECIPIENTS.join(' ')} this merge request touches files that could potentially affect user growth or subscription cost management.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def growth_affecting_change?
      interesting_files = paths_for_owner(CODEOWNERS_ENTRY) | GROWTH_ALERT_FILES
      changed_file_list.any_change?(interesting_files)
    end
  end
end
