# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class CodeReviewExperienceFeedback < CommunityProcessor
    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      wider_community_contribution? &&
        unique_comment.no_previous_comment? &&
        event.not_spam?
    end

    def process
      post_review_experience_comment
    end

    def documentation
      <<~TEXT
        This processor asks for feedback from community contributors about the code review experience.
      TEXT
    end

    private

    def post_review_experience_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      duo_access =
        if event.from_community_fork?
          'As a benefit of being a GitLab Community Contributor, you have access to GitLab Duo, our AI-powered features.'
        else
          '[Request access](https://gitlab.com/groups/gitlab-community/community-members/-/group_members/request_access) to our community forks to receive complimentary access to GitLab Duo, our AI-powered features.'
        end

      contributor_platform_message = "\n🎉 See where you rank! Check your spot on the [contributor leaderboard](https://contributors.gitlab.com/leaderboard/me) and unlock rewards.\n"
      contributor_platform_leader = event.resource_merged? ? "#{contributor_platform_message}\n---\n" : ''
      contributor_platform_trailer = event.resource_merged? ? '' : contributor_platform_message

      comment = <<~MARKDOWN.chomp
        Hi @#{event.resource_author.username} :wave:
        #{contributor_platform_leader}
        How was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://handbook.gitlab.com/handbook/values/#iteration) and improve:

        1. React with a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
        1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.

        #{duo_access}
        With Code Suggestions, Chat, Root Cause Analysis and more AI-powered features, GitLab Duo helps to boost your efficiency and effectiveness
        by reducing the time required to write and understand code and pipelines.
        Visit the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/gitlab_duo/) to learn more about the benefits.
        #{contributor_platform_trailer}
        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end

    def not_spam?
      !event.label_names.include?(Labels::SPAM_LABEL)
    end
  end
end
