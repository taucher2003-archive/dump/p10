# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  module Workflow
    class NewIssueWorkflowLabeller < Processor
      include Labels

      react_to 'issue.*'

      def applicable?
        event.from_gitlab_org? &&
          event.project_public? &&
          new_issue_workflow_label != current_issue_workflow_label
      end

      def process
        label_command = "/label ~#{new_issue_workflow_label}"
        add_comment(label_command, append_source_link: false)
      end

      def documentation
        <<~TEXT
        This processor applies new ~issue:: scoped workflow labels based on old ~workflow:: labels
        TEXT
      end

      def new_issue_workflow_label
        case legacy_workflow_label
        when nil, *NEW_WORKFLOW_VALIDATION
          ISSUE_VALIDATION_LABEL
        when *NEW_WORKFLOW_PLANNING
          ISSUE_PLANNING_LABEL
        when *NEW_WORKFLOW_DEVELOPMENT
          ISSUE_DEVELOPMENT_LABEL
        when *NEW_WORKFLOW_COMPLETE
          ISSUE_COMPLETE_LABEL
        else
          ISSUE_VALIDATION_LABEL
        end
      end

      def current_issue_workflow_label
        event.label_names.find { |label| label.start_with?('issue::') }
      end

      def legacy_workflow_label
        event.label_names.find { |label| label.start_with?('workflow::') }
      end
    end
  end
end
