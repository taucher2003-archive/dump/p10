# frozen_string_literal: true

require_relative './base_job'

module Triage
  module PipelineIncidentSla
    class StageWarningJob < BaseJob
      DELAY = (3 * 60 * 60) + (40 * 60) # 3 hours and 40 minutes
      MINUTES_BEFORE_ESCALATION = 20

      ATTRIBUTION_COMMENT_TMEPLATE = <<~MESSAGE.chomp
        I'm requesting help on behalf of `#%<group_channel>s` to triage a recurring pipeline incident <%<incident_url>s|#%<incident_iid>s>.
      MESSAGE

      WEEKDAY_ESCALATION_COMMENT_TEMPLATE = <<~MESSAGE.chomp
        The incident has not received any update and will be escalated to <#%<dev_escalation_channel_id>s> in %<minutes_before_escalation>s minutes of inactivity (except weekends and holidays).
      MESSAGE

      WEEKEND_ESCALATION_COMMENT_TEMPLATE = <<~MESSAGE.chomp
        If it is blocking any urgent deployment or merge request for the weekend, please escalate to <#%<dev_escalation_channel_id>s>.
        Otherwise, please help triage the incident on your next working day.
      MESSAGE

      private

      def applicable?
        super && attributed_group
      end

      def slack_channel
        stage_slack_channel
      end

      def attribution_comment_body
        format(ATTRIBUTION_COMMENT_TMEPLATE,
          group_channel: group_slack_channel,
          incident_iid: event.iid,
          incident_url: event.url
        )
      end

      def escalation_comment_body
        if today_is_weekend?
          format(WEEKEND_ESCALATION_COMMENT_TEMPLATE,
            dev_escalation_channel_id: DEV_ESCALATION_CHANNEL_ID
          )
        else
          format(WEEKDAY_ESCALATION_COMMENT_TEMPLATE,
            dev_escalation_channel_id: DEV_ESCALATION_CHANNEL_ID,
            minutes_before_escalation: MINUTES_BEFORE_ESCALATION
          )
        end
      end

      def slack_message
        <<~MESSAGE.chomp
          #{attribution_comment_body}
          #{escalation_comment_body}
          Thanks for your attention!
        MESSAGE
      end
    end
  end
end
